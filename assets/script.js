//import {createGitgraph} from "@gitgraph/js";

let buttonElement = document.getElementById("repo_request");
buttonElement.addEventListener(onclick, queryRepo);

function checkRepo(repoIdElement) {
    let query = {};
    fetch("https://gitlab.com/api/v4/projects/" + repoIdElement.value + "/repository/branches")
        .then(response => response.json())
        .then(data => query = data);
    console.log(query);
    return typeof (query.message) === 'undefined';
}

function queryRepo() {
    let repoIDElement = document.getElementById("repo_id");
    if (!checkRepo(repoIDElement)) {
        alert("Repo ID is not valid, can't access repo on GitLab.com");
    } else {
        const graphContainer = document.getElementById("gitgraph");
        const gitgraph = createGitgraph(graphContainer);
        display(organizeRepo(loadRepo(repoIDElement.value)), gitgraph);
    }
}

function loadRepo(repoId) {
    const repoUrl = "https://gitlab.com/api/v4/projects/" + repoId + "/repository/";
    const repoBranches = JSON.parse(repoUrl + "branches");
    const repoFiles = JSON.parse(repoUrl + "tree");
    return [repoUrl, repoBranches, repoFiles];
}

function organizeRepo(stackRepo) {
    const repoUrl = stackRepo[0];
    let repoBranches = [];
    for (let i = 0; i < stackRepo[1].length; i++) {
        repoBranches[i].name = stackRepo[i].name;
        repoBranches[i].commits = JSON.parse(repoUrl + "commits?all=1&ref_name=" + stackRepo[i].name);
    }
    return [repoUrl, repoBranches, stackRepo[2]];
}

function display(stackRepo, gitgraph) {
    let repoBranches = stackRepo[1];
    for (let i = 0; i < repoBranches; i++) {
        let branch = gitgraph.branch(repoBranches[i].name);
        for (let j = 0; j < repoBranches[i].commits; j++) {
            let commit = {};
            commit.author.name = repoBranches[i].commits[j].author_name;
            commit.author.email = repoBranches[i].commits[j].author_email;
            commit.hash = repoBranches[i].commits[j].id;
            commit.hashAbbrev = repoBranches[i].commits[j].short_id;
            commit.subject = repoBranches[i].commits[j].title;
            commit.body = repoBranches[i].commits[j].message;
            branch.commit(commit);
        }
    }
}