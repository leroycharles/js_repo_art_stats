# Repo Art And Stats
## Objectives
- Represent branches and commits from a GitLab repository
- Express the knowledge acquired through the JS class at Paris Ynov Campus
## Context
This project is a school project.

## Resources
- GitLab API Doc
- GraphQl
- GitGraphJS

## Components
- Authentication
- Collect
- Organize
- Display